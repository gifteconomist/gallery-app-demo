import React from 'react';
import PropTypes from 'prop-types';

import ShowList from './ShowList.jsx';
import ShowCurrent from './ShowCurrent.jsx';

export default class Shows extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showIndex: props.shows.map(function(show) {
          return (
            show.id
          )
      }).indexOf(props.clientId)
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      showIndex: nextProps.shows.map(function(show) {
          return (
            show.id
          )
      }).indexOf(nextProps.clientId)
    });
  }

  render() {
    const { shows, clientId } = this.props;

    //index of clientId
    var showIndex = shows.map(function(show) {
        return (
          show.id
        )
    }).indexOf(clientId);

    let sliderSettings = {
      infinite: true,
      speed: 500,
      centerMode: true,
      slidesToShow: 9,
      arrows: false,
      initialSlide: showIndex + 1,
      slickGoTo: showIndex + 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
          }
        },
        {
          breakpoint: 780,
          settings: {
            slidesToShow: 3,
          }
        },
      ],
    }

    return (
      <div>
        <ShowList showIndex={this.state.showIndex} clientId={clientId} shows={shows}/>
        <ShowCurrent showIndex={this.state.showIndex} shows={shows}></ShowCurrent>
      </div>
    );
  }
}

Shows.propTypes = {
  clientId: PropTypes.number,
  shows: PropTypes.array
}
