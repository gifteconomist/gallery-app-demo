import React from 'react';
import ReactDOM from 'react-dom';

import ReactRouter from 'react-router';

{/*react-router-dom new to v4*/}
import { BrowserRouter, Match, Route, Switch } from 'react-router-dom';

import Gallery from './Gallery.jsx';

import SHOWS from '../shows.json';

export default class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
          <Route exactly pattern="/" component={Gallery} />
      </BrowserRouter>
    );
  }
}
