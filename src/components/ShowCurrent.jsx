import React from 'react';
import PropTypes from 'prop-types';

export default class ShowCurrent extends React.Component {

  render() {
    console.log(this.props)

    const { shows, showIndex, } = this.props;
    const showCurrent = shows[this.props.showIndex];

    return (
      <main key={showCurrent.id}>
        <div className="show-info">
          <div className="show-info--text col">
            <h2>{showCurrent.title}</h2>
            <h3>{showCurrent.episodes} Episodes</h3>
            <p>Rapper Action Bronson travels the world performing with friends, and trying new foods.</p>
          </div>
          <div className="show-info--image col">
            <img src={"../src" + showCurrent.product_image_url} alt={showCurrent.title}/>
          </div>
        </div>
      </main>
    );
  }
}

ShowCurrent.propTypes = {
  showIndex: PropTypes.number,
  shows: PropTypes.array
}
