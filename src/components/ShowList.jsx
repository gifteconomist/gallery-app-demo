import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Slider from 'react-slick';

export default class ShowList extends React.Component {
  render() {

    const { shows, showIndex, clientId } = this.props;

    //set href and and add active component
    var showList = shows.map(function(show) {
      return (
        <Link
          to={`?id=${show.id}`}
          className={`show-item ${show.id === clientId ? 'active' : '' }`}
          key={show.id}
        >
          <img src={`../src${show.product_image_url}`} alt={show.title}/>
        </Link>
      )
    });

    let sliderSettings = {
      infinite: true,
      speed: 500,
      centerMode: true,
      slidesToShow: 9,
      arrows: false,
      initialSlide: showIndex + 1,
      slickGoTo: showIndex + 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
          }
        },
        {
          breakpoint: 780,
          settings: {
            slidesToShow: 3,
          }
        },
      ],
    }

    return (
      <nav className="show-list">
        <Slider ref='slider' {...sliderSettings}>
          {showList}
        </Slider>
        <Link
          to={`?id=${(showIndex > 0) ? shows[showIndex-1].id : shows.length}`}
          className="arrow arrow--left"
        >&#8592;</Link>
        <Link
          to={`?id=${(showIndex < shows.length-1) ? shows[showIndex+1].id : shows[0].id}`}
          className="arrow arrow--right"
        >&#8594;</Link>
      </nav>
    );
  }
}

ShowList.propTypes = {
  showIndex: PropTypes.number,
  clientId: PropTypes.number,
  shows: PropTypes.array
}
