import React from 'react';

import Shows from './Shows.jsx';

import SHOWS from '../shows.json';

export default class Gallery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clientId: this.getQueryId(props.location)
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      clientId: this.getQueryId(nextProps.location)
    });
  }

  getQueryId(location) {
    const query = location.search;
    const parseQuery = parseInt(query.substring(query.indexOf("?") + "?id=".length));
    const id = parseQuery ? parseQuery : 1;

    return parseInt(id);
  }

  render() {
    return (
      <div>
        <header><h1>VICELAND</h1></header>
        <Shows shows={SHOWS} clientId={this.state.clientId}/>
        <footer><p>VICE DEMO built by Grace</p></footer>
      </div>
    );
  }
}
